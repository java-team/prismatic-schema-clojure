Source: prismatic-schema-clojure
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Build-Depends: clojure (>= 1.8),
               debhelper (>= 10),
               default-jdk,
               javahelper (>= 0.32),
               markdown,
               maven-repo-helper (>= 1.5~)
Standards-Version: 4.0.0
Vcs-Git: https://anonscm.debian.org/git/pkg-java/prismatic-schema-clojure.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-java/prismatic-schema-clojure.git
Homepage: https://github.com/plumatic/schema

Package: libprismatic-schema-clojure
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Recommends: ${java:Recommends}
Description: Clojure(Script) library for declarative data description and validation
 Schema is a rich language for describing data shapes, with a variety of
 features:
 .
  * Data validation, with descriptive error messages of failures (targeted at
    programmers)
  * Annotation of function arguments and return values, with optional runtime
    validation
  * Schema-driven data coercion, which can automatically, succinctly, and
    safely convert complex data types.
